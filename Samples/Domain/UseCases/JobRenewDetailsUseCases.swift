//
//  JobRenewDetailsUseCases.swift
//  Samples
//
//  Created by Eslam Ahmad on 8/21/19.
//  Copyright © 2019 Eslam Ahmad. All rights reserved.
//

import Foundation
import RxSwift

func renewJobDetails(jobID: String , dataSource :OrcasApiDataSource )-> Single<JobRenewDetailsScreenData>{
    return dataSource.renewJobDetails(jobID: jobID)
        .map{response in
            let jobDetails = response.data.jobDetails
            return JobRenewDetailsScreenData(
                jobEndedAt: jobDetails.jobEndedAt, serviceName: jobDetails.serviceName, focusValue: jobDetails.focusValue, loading: false
            )
    }
}

///Just stub method, you must replace with acctual impl
func validateSessionStartDate(startDate:String)-> Bool {
    return true
}
// ..... The rest of use cases
