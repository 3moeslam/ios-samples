//
//  OrcasApiDataSource.swift
//  Samples
//
//  Created by Eslam Ahmad on 8/21/19.
//  Copyright © 2019 Eslam Ahmad. All rights reserved.
//

import Foundation
import RxSwift


protocol OrcasApiDataSource {
    func renewJobDetails(jobID: String)-> Single<JobRenewDetailsResponse>
}

