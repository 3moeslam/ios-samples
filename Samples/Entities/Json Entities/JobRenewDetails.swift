//
//  JobRenewDetails.swift
//  Samples
//
//  Created by Eslam Ahmad on 8/21/19.
//  Copyright © 2019 Eslam Ahmad. All rights reserved.
//

import Foundation
// MARK: - Welcome
struct JobRenewDetailsResponse: Codable {
    let status: Status
    let data: DataClass
}

// MARK: - DataClass
struct DataClass: Codable {
    let jobDetails: JobDetails
}

// MARK: - JobDetails
struct JobDetails: Codable {
    let jobID, jobEndedAt, serviceName, focusValue: String
    let areaName: String
    let weeksNumber, sessionsNumber, sessionsHours: Int
    let address: Address
    let partner: Partner
    let children: [Child]
    let canPayLater: Bool
    
    enum CodingKeys: String, CodingKey {
        case jobID = "jobId"
        case jobEndedAt, serviceName, focusValue, areaName, weeksNumber, sessionsNumber, sessionsHours, address, partner, children, canPayLater
    }
}

// MARK: - Address
struct Address: Codable {
    let addressName: String
}

// MARK: - Child
struct Child: Codable {
    let name: String
    let image: String
}

// MARK: - Partner
struct Partner: Codable {
    let name: String
    let image: String
    let rate: Double
    let partnerID, partnerProfileID: String
    let partnerHourPrice: Int
    let partnerPriceCurrency: String
    
    enum CodingKeys: String, CodingKey {
        case name, image, rate
        case partnerID = "partnerId"
        case partnerProfileID = "partnerProfileId"
        case partnerHourPrice, partnerPriceCurrency
    }
}

// MARK: - Status
struct Status: Codable {
    let code: Int
    let message: String
}
