//
//  JobRenewDetails.swift
//  Samples
//
//  Created by Eslam Ahmad on 8/21/19.
//  Copyright © 2019 Eslam Ahmad. All rights reserved.
//

import Foundation

struct JobRenewDetailsScreenData {
    let jobEndedAt :String
    let serviceName:String
    let focusValue :String
    
    let loading :Bool
    // And rest of screen values
    
    static func empty()->JobRenewDetailsScreenData{
        return JobRenewDetailsScreenData(jobEndedAt: "", serviceName: "", focusValue: "", loading: true)
    }
}
