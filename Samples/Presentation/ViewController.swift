//
//  ViewController.swift
//  Samples
//
//  Created by Eslam Ahmad on 8/20/19.
//  Copyright © 2019 Eslam Ahmad. All rights reserved.
//

import UIKit
import Moya
import RxSwift

class ViewController: UIViewController {

    private let viewModel = ViewModel()
    private let disposables = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.dataSubject.subscribe{
            //Draw data
        }.disposed(by: disposables)
        
        
        
        viewModel.loadData(jobID: "a1b79fe8-d6b9-4202-906f-31a7c796b1d8")
        
    }


}

