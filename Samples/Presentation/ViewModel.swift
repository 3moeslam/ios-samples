//
//  ViewModel.swift
//  Samples
//
//  Created by Eslam Ahmad on 8/21/19.
//  Copyright © 2019 Eslam Ahmad. All rights reserved.
//

import Foundation
import RxSwift


class ViewModel{
    
    let dataSubject = BehaviorSubject<JobRenewDetailsScreenData>(value: JobRenewDetailsScreenData.empty())
    
    private let disposables = DisposeBag()
    private let dataSource :OrcasApiDataSource
    private let loadingUseCase : (String,OrcasApiDataSource )->Single<JobRenewDetailsScreenData>
    private let dateValidationMethod :(String)-> Bool
    
    init(
        dataSource :OrcasApiDataSource = MoyaOrcasApiDataSource(),
        loadingUseCase : @escaping (String,OrcasApiDataSource )->Single<JobRenewDetailsScreenData> = renewJobDetails,
        dateValidationMethod : @escaping (String)-> Bool = validateSessionStartDate
    ) {
        self.dataSource = dataSource as! MoyaOrcasApiDataSource
        self.loadingUseCase = loadingUseCase
        self.dateValidationMethod = dateValidationMethod
    }
    
    
    func loadData(jobID:String){
        loadingUseCase( jobID , dataSource)
            .subscribe(onSuccess: { [weak self] result in
                self?.dataSubject.onNext(result)
            }, onError: { error in
                print(error)
                //Handle error
            }).disposed(by: disposables)
    }
    
    func validateDate(date:String) -> Bool {
        return dateValidationMethod(date)
    }
    //.... and so on
    
    
}
