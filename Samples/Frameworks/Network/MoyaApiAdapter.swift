//
//  MoyaApiAdapter.swift
//  Samples
//
//  Created by Eslam Ahmad on 8/21/19.
//  Copyright © 2019 Eslam Ahmad. All rights reserved.
//

import Foundation
import Moya
import RxSwift


class MoyaOrcasApiDataSource : OrcasApiDataSource{
    private let provider = MoyaProvider<OrcasApi>(plugins: [NetworkLoggerPlugin(verbose: true)])
    
    func renewJobDetails(jobID: String) -> Single<JobRenewDetailsResponse> {
        return provider.rx
            .request(.renewJobDetails(jobID))
            .filterSuccessfulStatusCodes()
            .map{try! JSONDecoder().decode(JobRenewDetailsResponse.self, from: $0.data)}
    }
    
    
    
}
