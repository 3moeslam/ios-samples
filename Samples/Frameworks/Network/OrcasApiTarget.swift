//
//  OrcasApi.swift
//  Samples
//
//  Created by Eslam Ahmad on 8/20/19.
//  Copyright © 2019 Eslam Ahmad. All rights reserved.
//

import Foundation
import Moya

///Implementation of Orcas Api
extension OrcasApi :TargetType {
    /// Switch method depend on Case
    var method: Moya.Method {
        switch self {
            case .renewJobDetails:
                return .get
            case .payLaterRenewJob:
                return .get
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    /// Switch task depend on Case
    var task: Task {
        switch self{
            case .renewJobDetails:
                return .requestPlain
            case .payLaterRenewJob:
                return .requestPlain
        }
    }
    
    /// Switch headers depend on Case
    var headers: [String : String]? {
        var parameters = [String: String]()
        parameters["Authorization"] = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VySWQiOiIwZjAyODA2MC00ZmY0LTRiZmEtYWNlMy1mZTAwODljNDYxNjkiLCJ1c2VyQXBwIjoidXNlciIsImlzcyI6Imh0dHBzOi8vb3JjYXNtb2JpbGUuY29tL2Rldi1vcmNhcy1jb3Jlcy9wdWJsaWMvYXBpX3YyL3VzZXIvbG9naW4iLCJpYXQiOjE1NjYzNDA2NTcsImV4cCI6Mjc2NjM0MDY1NywibmJmIjoxNTY2MzQwNjU3LCJqdGkiOiJuSFRZMEU3T3YxNjZ1ZDNNIn0.NmLd_ug568hwgr8lXP2mlUOV-cq5qCVXjpgIK9C3V7I"
        return parameters
    }
    
    /// Switch baseURL depend on Case
    var baseURL :URL {
        return URL(string : "https://orcasmobile.com/dev-orcas-cores/public/api_v2/user/")!
    }
    
    /// Switch baseURL depend on Case
    var path: String {
        switch self {
            case .renewJobDetails(let jobID):
                return "job/renew/details/\(jobID)"
            case .payLaterRenewJob:
                return ""
        }
    }
    
}
